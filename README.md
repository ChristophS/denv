# Install
Copy denv file & chmod +x to [denv](https://gitlab.com/ChristophS/denv/raw/master/denv) file

## One Command Installer
```
curl https://gitlab.com/ChristophS/denv/raw/master/denv --output denv; chmod +x denv
```
