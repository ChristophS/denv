from python:2.7.14-alpine3.7

# Check if changes in files 
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY requirements-dev.txt requirements-dev.txt
RUN pip install -r requirements-dev.txt

WORKDIR /workspace

ENTRYPOINT [ "python" ]
