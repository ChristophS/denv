from lib.hello_world import hello_world
from unittest import TestCase

class TestHelloWorld(TestCase):
    def test_returns_string(self):
        self.assertIsInstance(hello_world(), str)

    def test_string_matches(self):
        self.assertEqual(hello_world(), "Hello World")